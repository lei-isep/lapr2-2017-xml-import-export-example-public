# README #

This repository contains some examples on how to:
* convert a csv file (stats_to_xml.csv) to an XML file - described in method convertFromCSVtoXML();
* load model classes information from an XML file - this is demonstrated in method ensureConvertFromXmlFileWorks()

## Release Notes ##

**NOTE 1:** This is just an example/reference project and you **MUST** adapt your own project according to your needs.

**NOTE 2:** This project may containt more or less properties than asked for in LAPR2 Project Assignment and some data might me incorrect.