package lapr.project.utils;

import lapr.project.model.Topic;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXParseException;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;


/**
 * @author Nuno Bettencourt [nmb@isep.ipp.pt] on 29/05/16.
 */
public class XMLParserTest {
    /**
     * StringUtil variable to access utils for Strings.
     */
    private StringUtil stringUtil = new StringUtil();

    /**
     * Get OS independent line break.
     *
     * @return OS independent line break "%n".
     */
    private String getLineBreak() {
        return stringUtil.getLineBreak();
    }

    @Test
    public void ensureXMLElementExportToStringIsValid() throws Exception {
        String expected = "<topic>" + getLineBreak()
                + "<value>Doors</value>" + getLineBreak()
                + "</topic>" + getLineBreak();

        Topic topic = new Topic("Doors");
        Node node = topic.exportContentToXMLNode();

        XMLParser xmlParser = new XMLParser();
        String result = xmlParser.convertToString(node);

        Diff myDiffIdentical = DiffBuilder.compare(expected).withTest(result).ignoreComments().ignoreWhitespace().checkForIdentical().build();
        assertFalse(myDiffIdentical.hasDifferences());

    }

    @Test
    public void ensureXMLDocumentExportToStringIsValid() throws Exception {
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><topic>" + getLineBreak()
                + "<value>Doors</value>" + getLineBreak()
                + "</topic>" + getLineBreak();


        DocumentBuilderFactory factory
                = DocumentBuilderFactory.newInstance();

        //Create document builder
        DocumentBuilder builder = factory.newDocumentBuilder();

        //Obtain a new document
        Document document = builder.newDocument();

        //Create root element
        Element elementTopic = document.createElement("topic");

        //Create a sub-element
        Element elementValue = document.createElement("value");

        //Set the sub-element value
        elementValue.setTextContent("Doors");

        //Add sub-element to root element
        elementTopic.appendChild(elementValue);

        //Add root element to document
        document.appendChild(elementTopic);

        XMLParser xmlParser = new XMLParser();
        String result = xmlParser.convertToString(document);

        Diff myDiffIdentical = DiffBuilder.compare(expected).withTest(result).ignoreComments().ignoreWhitespace().checkForIdentical().build();
        assertFalse(myDiffIdentical.hasDifferences());

    }

    @Test
    public void readFromValidFile() throws Exception {
        DocumentBuilderFactory factory
                = DocumentBuilderFactory.newInstance();

        //Create document builder
        DocumentBuilder builder = null;
        builder = factory.newDocumentBuilder();

        //Obtain a new document
        Document document = builder.newDocument();

        //Create root element
        Element elementTopic = document.createElement("topic");

        //Create a sub-element
        Element elementValue = document.createElement("value");

        //Set the sub-element value
        elementValue.setTextContent("Doors");

        //Add sub-element to root element
        elementTopic.appendChild(elementValue);

        //Add root element to document
        document.appendChild(elementTopic);
        Node expected = document.getDocumentElement();

        String filename = "target/test-classes/TopicXMLValidExample.xml";
        XMLParser xmlParser = new XMLParser();

        Node result = xmlParser.readXMLElementFromFile(filename);

        Diff myDiffIdentical = DiffBuilder.compare(expected).withTest(result).ignoreComments().ignoreWhitespace().checkForIdentical().build();
        assertFalse(myDiffIdentical.hasDifferences());

    }

    @Test
    public void readFromInvalidFile() throws Exception {
        String filename = "target/test-classes/TopicXMLInvalidExample.xml";
        XMLParser xmlParser = new XMLParser();

        Throwable exception = assertThrows(SAXParseException.class, () -> {
            xmlParser.readXMLElementFromFile(filename);
        });

    }

    @Test
    public void readFromNonExistingFile() throws Exception {
        String filename = "InvalidFileName";
        XMLParser xmlParser = new XMLParser();

        Throwable exception = assertThrows(FileNotFoundException.class, () -> {
            xmlParser.readXMLElementFromFile(filename);
        });

    }

    @Test
    public void writeXMLElementToFile() throws Exception {
        DocumentBuilderFactory factory
                = DocumentBuilderFactory.newInstance();

        //Create document builder
        DocumentBuilder builder = null;
        builder = factory.newDocumentBuilder();

        //Obtain a new document
        Document document = builder.newDocument();

        //Create root element
        Element elementTopic = document.createElement("topic");

        //Create a sub-element
        Element elementValue = document.createElement("value");

        //Set the sub-element value
        elementValue.setTextContent("Doors");

        //Add sub-element to root element
        elementTopic.appendChild(elementValue);

        //Add root element to document
        document.appendChild(elementTopic);
        Node expected = document.getDocumentElement();

        String filename = "target/test-classes/ExampleOutput.xml";
        XMLParser xmlParser = new XMLParser();

        xmlParser.writeXMLElementToFile(expected, filename);

        Node result = xmlParser.readXMLElementFromFile(filename);

        Diff myDiffIdentical = DiffBuilder.compare(expected).withTest(result).ignoreComments().ignoreWhitespace().checkForIdentical().build();
        assertFalse(myDiffIdentical.hasDifferences());

    }

}
