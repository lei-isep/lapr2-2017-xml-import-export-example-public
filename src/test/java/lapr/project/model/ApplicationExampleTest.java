package lapr.project.model;

import lapr.project.utils.StringUtil;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class to demonstrate a Application simple example.
 *
 * @author Nuno Bettencourt [nmb@isep.ipp.pt] on 29/05/16.
 */
public class ApplicationExampleTest {

    /**
     * StringUtil variable to access utils for Strings.
     */
    private StringUtil stringUtil = new StringUtil();

    /**
     * Get OS independent line break.
     *
     * @return OS independent line break "%n".
     */
    private String getLineBreak() {
        return stringUtil.getLineBreak();
    }

    @Test
    public void ensureAddTopicIsWorking() throws Exception {
        List<Topic> expectedTopicList = new ArrayList<>();
        expectedTopicList.add(new Topic("Doors"));

        AnotherApplicationExample candidatura = new AnotherApplicationExample("MyCandidatura", new ArrayList<>());
        candidatura.addTopic(new Topic("Doors"));

        List<Topic> resultList = candidatura.getTopicList();

        assertArrayEquals(expectedTopicList.toArray(), resultList.toArray());

    }

    @Test
    public void ensureXMLElementExportToStringIsValid() throws Exception {
        String expected = "<application>" + getLineBreak() +
                "<description>MyCandidatura</description>" + getLineBreak() +
                "<topics>" + getLineBreak() +
                "<topic>" + getLineBreak() +
                "<value>Doors</value>" + getLineBreak() +
                "</topic>" + getLineBreak() +
                "<topic>" + getLineBreak() +
                "<value>Windows</value>" + getLineBreak() +
                "</topic>" + getLineBreak() +
                "</topics>" + getLineBreak() +
                "</application>" + getLineBreak();

        List<Topic> topicList = new ArrayList<>();
        topicList.add(new Topic("Doors"));
        topicList.add(new Topic("Windows"));
        AnotherApplicationExample anotherApplicationExample = new AnotherApplicationExample("MyCandidatura", topicList);
        String result = anotherApplicationExample.exportContentToString();
        assertEquals(expected, result);
    }

    @Test
    public void ensureImportFromXMLElementNodeIsValid() throws Exception {
        List<Topic> topicList = new ArrayList<>();
        topicList.add(new Topic("Doors"));
        topicList.add(new Topic("Windows"));

        AnotherApplicationExample expected = new AnotherApplicationExample("MyCandidatura", topicList);

        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();

        //Create document builder
        DocumentBuilder builder = factory.newDocumentBuilder();

        //Obtain a new document
        Document document = builder.newDocument();

        //Create root element
        Element elementCandidatura = document.createElement("application");

        //Create a sub-element
        Element elementDescription = document.createElement("description");

        //Set the sub-element value
        elementDescription.setTextContent("MyCandidatura");

        //Add sub-element to root element
        elementCandidatura.appendChild(elementDescription);

        //Create a sub-element
        Element elementTopics = document.createElement("topics");

        //iterate over topics
        for (Topic topic : topicList) {
            Node topicNode = topic.exportContentToXMLNode();
            elementTopics.appendChild(document.importNode(topicNode, true));
        }

        elementCandidatura.appendChild(elementTopics);

        //Add root element to document
        document.appendChild(elementCandidatura);

        AnotherApplicationExample result = new AnotherApplicationExample();
        result = result.importContentFromXMLNode(elementCandidatura);

        assertEquals(expected, result);
    }

    @Test
    public void ensureSameContentObjectsAreEqual() {
        String description = "MyCandidatura";

        List<Topic> topics = new ArrayList<>();
        topics.add(new Topic("Doors"));
        topics.add(new Topic("Windows"));

        AnotherApplicationExample expected = new AnotherApplicationExample(description, topics);
        AnotherApplicationExample result = new AnotherApplicationExample(description, topics);

        assertEquals(expected, result);
    }

    @Test
    public void ensureSameObjectIsEqual() {
        String description = "MyCandidatura";

        List<Topic> topics = new ArrayList<>();
        topics.add(new Topic("Doors"));
        topics.add(new Topic("Windows"));

        AnotherApplicationExample expected = new AnotherApplicationExample(description, topics);

        assertEquals(expected, expected);
    }

    @Test
    public void ensureDifferentObjectsAreNotEqual() {
        String description = "MyCandidatura";

        List<Topic> topics = new ArrayList<>();
        topics.add(new Topic("Doors"));
        topics.add(new Topic("Windows"));

        AnotherApplicationExample expected = new AnotherApplicationExample(description, topics);

        Object result = new Object();
        assertNotEquals(expected, result);
    }

    @Test
    public void ensureDifferentDescriptionMakeObjectsNotEqual() {
        String description1 = "MyCandidatura1";
        String description2 = "MyCandidatura2";

        List<Topic> topics = new ArrayList<>();
        topics.add(new Topic("Doors"));
        topics.add(new Topic("Windows"));

        AnotherApplicationExample expected = new AnotherApplicationExample(description1, topics);
        AnotherApplicationExample result = new AnotherApplicationExample(description2, topics);

        assertNotEquals(expected, result);
    }

    @Test
    public void ensureHashCodeIsCorrect() {
        String description = "MyCandidatura";

        List<Topic> topics = new ArrayList<>();
        topics.add(new Topic("Doors"));
        topics.add(new Topic("Windows"));

        AnotherApplicationExample anotherApplicationExample = new AnotherApplicationExample(description, topics);

        int expected = 461375881;
        int result = anotherApplicationExample.hashCode();
        assertEquals(expected, result);

    }


}