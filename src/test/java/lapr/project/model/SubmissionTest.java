package lapr.project.model;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by nuno on 28/05/17.
 */
public class SubmissionTest {

    @Test
    public void ensureTheSameSubmissionIsConsideredEqual() throws ParseException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date dataInicioSubmissao = simpleDateFormat.parse("2007-01-01 00:00:00");
        Date dataFimSubmissao = simpleDateFormat.parse("2007-02-01 00:00:00");

        Submission expectedResult = new Submission(dataInicioSubmissao, dataFimSubmissao);

        Submission submission = new Submission(dataInicioSubmissao, dataFimSubmissao);
        assertEquals(submission, submission);

    }

    @Test
    public void ensureTwoDifferentSubmissionsAreConsideredEqual() throws ParseException {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date dataInicioSubmissao = simpleDateFormat.parse("2007-01-01 00:00:00");
        Date dataFimSubmissao = simpleDateFormat.parse("2007-02-01 00:00:00");

        Submission oneSubmission = new Submission(dataInicioSubmissao, dataFimSubmissao);
        Submission anotherSubmission = new Submission(dataInicioSubmissao, dataFimSubmissao);

        assertEquals(oneSubmission, anotherSubmission);
    }

}