package lapr.project.model;

import lapr.project.utils.XMLParser;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Node;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
public class OrganiserTest {
    @Test
    public void ensureConvertToXmlWorks() throws Exception {
        String utilizadorExportFilePath = "target/test-classes/OrganizadorOutput.xml";
        String expectedUtilizadorImportFilePath = "target/test-classes/OrganizadorImportExample.xml";

        User user = new User("João", "joao@empresa.pt", "joao@empresa.pt", "password");
        Organiser organiser = new Organiser(user);

        File file = new File(utilizadorExportFilePath);
        JAXBContext jaxbContext = JAXBContext.newInstance(Organiser.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(organiser, file);
        jaxbMarshaller.marshal(organiser, System.out);

        XMLParser xmlParser = new XMLParser();

        Node expected = xmlParser.readXMLElementFromFile(utilizadorExportFilePath);

        Node result = xmlParser.readXMLElementFromFile(expectedUtilizadorImportFilePath);

        Diff myDiffIdentical = DiffBuilder.compare(expected).withTest(result).ignoreComments().ignoreWhitespace().checkForIdentical().build();
        assertFalse(myDiffIdentical.hasDifferences());

    }

    @Test
    public void ensureConvertToObjectWorks() throws Exception {
        String expectedUtilizadorImportFilePath = "target/test-classes/OrganizadorImportExample.xml";

        User user = new User("João", "joao@empresa.pt", "joao@empresa.pt", "password");
        Organiser expected = new Organiser(user);

        File file = new File(expectedUtilizadorImportFilePath);
        JAXBContext jaxbContext = JAXBContext.newInstance(Organiser.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Organiser result = (Organiser) jaxbUnmarshaller.unmarshal(file);

        assertEquals(expected, result);
    }
}