package lapr.project.model;

import lapr.project.utils.StringUtil;
import lapr.project.utils.XMLParser;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Example of a domain class that is used in Application.
 * Created by Nuno Bettencourt [NMB] on 29/05/16.
 */
public class TopicTest {
    /**
     * StringUtil variable to access utils for Strings.
     */
    private StringUtil stringUtil = new StringUtil();

    /**
     * Get OS independent line break.
     *
     * @return OS independent line break "%n".
     */
    private String getLineBreak() {
        return stringUtil.getLineBreak();
    }

    @Test
    public void ensureSameContentObjectsAreEqual() {
        Topic expected = new Topic("Doors");
        Topic result = new Topic("Doors");
        assertEquals(expected, result);
    }

    @Test
    public void ensureSameObjectIsEqual() {
        Topic expected = new Topic("Doors");
        assertEquals(expected, expected);
    }

    @Test
    public void ensureDifferentObjectsAreNotEqual() {
        Topic expected = new Topic("Doors");
        Object result = new Object();
        assertNotEquals(expected, result);
    }

    @Test
    public void ensureHashCodeIsCorrect() {
        Topic firstTopic = new Topic("Doors");

        int expected = 66216549;
        int result = firstTopic.hashCode();
        assertEquals(expected, result);
    }

    @Test
    public void ensureXMLElementExportToStringIsValid() throws Exception {
        String expected = "<topic>" + getLineBreak() +
                "<value>Doors</value>" + getLineBreak() +
                "</topic>" + getLineBreak();
        Topic topic = new Topic("Doors");
        String result = topic.exportContentToString();
        assertEquals(expected, result);
    }

    @Test
    public void ensureXMLElementExportToNodeIsValid() throws Exception {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        Node expected = null;

        //Create document builder
        DocumentBuilder builder = factory.newDocumentBuilder();

        //Obtain a new document
        Document document = builder.newDocument();

        //Create root element
        Element elementTopic = document.createElement("topic");

        //Create a sub-element
        Element elementValue = document.createElement("value");

        //Set the sub-element value
        elementValue.setTextContent("Doors");

        //Add sub-element to root element
        elementTopic.appendChild(elementValue);

        //Add root element to document
        document.appendChild(elementTopic);

        expected = elementTopic;


        Topic topic = new Topic("Doors");

        Node result = topic.exportContentToXMLNode();
        assertTrue(expected.isEqualNode(result));
    }

    @Test
    public void ensureImportFromXMLElementNodeIsValid() throws Exception {
        Topic expected = new Topic("Doors");

        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();

        //Create document builder
        DocumentBuilder builder = factory.newDocumentBuilder();

        //Obtain a new document
        Document document = builder.newDocument();

        //Create root element
        Element elementTopic = document.createElement("topic");

        //Create a sub-element
        Element elementValue = document.createElement("value");

        //Set the sub-element value
        elementValue.setTextContent("Doors");

        //Add sub-element to root element
        elementTopic.appendChild(elementValue);

        //Add root element to document
        document.appendChild(elementTopic);

        Topic topic = new Topic("Doors");

        Topic result = topic.importContentFromXMLNode(elementTopic);

        assertEquals(expected, result);
    }

    @Test
    public void testCreateExportImport() throws Exception {
        String filename = "target/test-classes/TestTopicImportExport.xml";
        Topic expected = new Topic("Doors");

        Node memoryNode = expected.exportContentToXMLNode();

        XMLParser xmlParser = new XMLParser();
        xmlParser.writeXMLElementToFile(memoryNode, filename);

        Node fileNode = xmlParser.readXMLElementFromFile(filename);

        Topic result = new Topic();
        result = result.importContentFromXMLNode(fileNode);

        assertEquals(expected, result);
    }
}