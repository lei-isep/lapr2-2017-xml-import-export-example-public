package lapr.project.model.converter;

import lapr.project.model.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by nuno on 13/06/17.
 */
public class EventSetTest {
    private User getUtilizador(CSVRecord record, String propertySuffix) {
        String nome = record.get("name" + propertySuffix);
        String email = record.get("email" + propertySuffix);
        String username = record.get("username" + propertySuffix);
        String password = record.get("password" + propertySuffix);

        return new User(nome, email, username, password);
    }

    private User getUtilizador2(CSVRecord record) {
        return getUtilizador(record, "2");
    }

    private User getUtilizador1(CSVRecord record) {
        return getUtilizador(record, "");
    }

    private Review getAvaliacao(CSVRecord record, String propertySuffix) {
        String texto = record.get("text" + propertySuffix);
        Integer conhecimento = Integer.parseInt(record.get("staffTopicKnowledge" + propertySuffix));
        Integer exposicao = Integer.parseInt(record.get("eventAdequacy" + propertySuffix));
        Integer convites = Integer.parseInt(record.get("inviteAdequacy" + propertySuffix));
        Integer recomendacao = Integer.parseInt(record.get("recommendation" + propertySuffix));

        Staff staff = new Staff(getUtilizador(record, propertySuffix));

        Assignment assignment = new Assignment(staff);//, candidatura1);

        return new Review(texto, conhecimento, exposicao, convites, recomendacao, assignment);
    }

    private Review getAvaliacao1(CSVRecord record) {
        return getAvaliacao(record, "");
    }

    private Review getAvaliacao2(CSVRecord record) {
        return getAvaliacao(record, "2");
    }

    private List<String> getTopics(CSVRecord record) {
        String topics = record.get("topics");
        List<String> topicList = new ArrayList<String>();
        for (String topic : StringUtils.split(topics, ",")) {
            topicList.add(topic);
        }
        return topicList;
    }

    private Application getCandidatura(CSVRecord record) {
        Boolean aceite = new Boolean(Boolean.valueOf(record.get("accepted")));
        String descricao = record.get("description");
        Integer area = Integer.parseInt(record.get("boothArea"));
        Integer convites = Integer.parseInt(record.get("invitesQuantity"));

        List<String> topics = getTopics(record);
        Application application = new Application(aceite, descricao, area, convites, new ArrayList<>(), topics);

        ArrayList<Review> avaliacoesList = new ArrayList<>();

        avaliacoesList.add(getAvaliacao1(record));
        avaliacoesList.add(getAvaliacao2(record));

        application.setReviews(avaliacoesList);
        return application;
    }

    private Stand getStand(CSVRecord record) {
        String standDescription = record.get("description");
        Integer area = Integer.parseInt(record.get("area"));

        Stand stand = new Stand(standDescription, area);

        return stand;
    }

    private Stands getStands() throws IOException {
        String standsFile = "target/test-classes/exposicao1_stands_v0.1.csv";
        Reader in = new FileReader(standsFile);
        Reader reader = new InputStreamReader(new FileInputStream(standsFile), "UTF-8");

        final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader().withDelimiter(';'));

        // Criar stands
        List<Stand> standsList = new ArrayList<Stand>();

        for (CSVRecord record : parser) {
            Stand stand = getStand(record);
            standsList.add(stand);
        }
        return new Stands(standsList);
    }

    @Test
    public void convertFromCSVtoXML() throws IOException, JAXBException, ParseException {
        String eventFile = "target/test-classes/exposicao1_v0.1.csv";
        String xmlFile = "target/test-classes/exposicao1_v0.1.xml";

        Reader in = new FileReader(eventFile);

        Reader reader = new InputStreamReader(new FileInputStream(eventFile), "UTF-8");

        final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader().withDelimiter(';'));

        // Criar exposição
        String tituloExposicao = new String("ExpoJaneiro");
        String descricaoExposicao = new String("ExpoJaneiro");

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);

        List<User> utilizadoresConfirmadosList = new ArrayList<>();

        List<Application> listaApplications = new ArrayList<>();

        ApplicationSet applicationSet = new ApplicationSet(listaApplications);

        User userOrganizador = new User("Manuel", "manuel@centro.pt", "manuel@centro.pt", "password");
        utilizadoresConfirmadosList.add(userOrganizador);

        List<Organiser> organizadoresList = new ArrayList<>();
        Organiser organiser = new Organiser(userOrganizador);
        organizadoresList.add(organiser);
        OrganiserSet organizadores = new OrganiserSet(organizadoresList);

        // Lista de StaffSet
        List<Staff> staffsList = new ArrayList<>();

        User staffUser1 = new User("Staff1", "staff1@centro.pt", "staff1@centro.pt", "password");
        utilizadoresConfirmadosList.add(staffUser1);

        User staffUser2 = new User("Staff2", "staff2@centro.pt", "staff2@centro.pt", "password");
        utilizadoresConfirmadosList.add(staffUser2);

        User staffUser3 = new User("Staff3", "staff3@centro.pt", "staff3@centro.pt", "password");
        utilizadoresConfirmadosList.add(staffUser3);

        User staffUser4 = new User("Staff4", "staff4@centro.pt", "staff4@centro.pt", "password");
        utilizadoresConfirmadosList.add(staffUser4);

        User staffUser5 = new User("Staff5", "staff5@centro.pt", "staff5@centro.pt", "password");
        utilizadoresConfirmadosList.add(staffUser5);

        Staff staff1 = new Staff(staffUser1);
        staffsList.add(staff1);
        Staff staff2 = new Staff(staffUser2);
        staffsList.add(staff2);
        Staff staff3 = new Staff(staffUser3);
        staffsList.add(staff3);
        Staff staff4 = new Staff(staffUser4);
        staffsList.add(staff4);
        Staff staff5 = new Staff(staffUser5);
        staffsList.add(staff5);
        StaffSet StaffSet = new StaffSet(staffsList);

        // Lista de stands
        Stands stands = getStands();

        Event event1 = new Event("exhibition1", StaffSet, new ApplicationSet(new ArrayList<>()), stands);

        //List<User> utilizadores = new ArrayList<>();

        for (CSVRecord record : parser) {
            Application application = getCandidatura(record);
            application.toString();

            String nomeExposicao = record.get("title");
            if ("exhibition1".equals(nomeExposicao)) {
                event1.addCandidatura(application);
            }
        }
        File file = new File(xmlFile);
        JAXBContext jaxbContext = JAXBContext.newInstance(Event.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        jaxbMarshaller.marshal(event1, file);
        //jaxbMarshaller.marshal(eventCentre, System.out);
    }
}