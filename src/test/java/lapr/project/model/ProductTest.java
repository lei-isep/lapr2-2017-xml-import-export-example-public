package lapr.project.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by nuno on 28/05/17.
 */
public class ProductTest {

    @Test
    public void ensureTwoProductsWithSameNameAreTheSame() {
        String productName = "Soap";
        Product oneProduct = new Product(productName);
        Product anotherProduct = new Product(productName);

        assertEquals(oneProduct, anotherProduct);
    }

    @Test
    public void ensureSameProductisEqual() {
        String productName = "Soap";
        Product oneProduct = new Product(productName);

        assertEquals(oneProduct, oneProduct);

    }
}