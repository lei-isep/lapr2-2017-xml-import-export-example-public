package lapr.project.model.converter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class EventSet {
	@XmlElement(name = "event")
	private List<Event> events;

	public EventSet(List<Event> events) {
		this.events = events;
	}

	private EventSet() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof EventSet)) {
			return false;
		}

		EventSet that = (EventSet) o;

		return events.equals(that.events);

	}

	@Override
	public int hashCode() {
		return events.hashCode();
	}
}
