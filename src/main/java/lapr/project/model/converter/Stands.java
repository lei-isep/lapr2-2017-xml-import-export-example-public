package lapr.project.model.converter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Stands {
	@XmlElement(name = "stand")
	private List<Stand> stands;

	private Stands() {
	}

	public Stands(List<Stand> stands) {

		this.stands = stands;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Stands)) {
			return false;
		}

		Stands stands1 = (Stands) o;

		return stands.equals(stands1.stands);

	}

	@Override
	public int hashCode() {
		return stands.hashCode();
	}
}
