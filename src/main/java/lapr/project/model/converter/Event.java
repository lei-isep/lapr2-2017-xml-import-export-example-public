package lapr.project.model.converter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lapr.project.model.StaffSet;
/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Event {
	@XmlElement
	private String title;

	@XmlElement
	private Stands stands;

	@XmlElement
	private StaffSet StaffSet;

	@XmlElement
	private ApplicationSet applicationSet;

	private Event() {
	}

	public Event(String title, StaffSet staffSet, ApplicationSet applicationSet, Stands stands) {
		this.title = title;
		this.StaffSet = staffSet;
		this.applicationSet = applicationSet;
		this.stands = stands;
	}

	public void addCandidatura(Application application) {
		applicationSet.addCandidatura(application);
	}

	@Override
	public int hashCode() {
		int result = title.hashCode();
		result = 31 * result + StaffSet.hashCode();
		result = 31 * result + applicationSet.hashCode();
		result = 31 * result + stands.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Event)) {
			return false;
		}

		Event exhibition = (Event) o;

		if (!title.equals(exhibition.title)) {
			return false;
		}
		if (!stands.equals(exhibition.stands)) {
			return false;
		}
		if (!StaffSet.equals(exhibition.StaffSet)) {
			return false;
		}
		return applicationSet.equals(exhibition.applicationSet);

	}
}
