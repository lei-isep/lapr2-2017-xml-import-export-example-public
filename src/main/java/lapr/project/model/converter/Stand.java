package lapr.project.model.converter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Stand {

	@XmlElement
	private String description;

	@XmlElement
	private Integer area;

	public Stand(String description, Integer area) {
		this.description = description;
		this.area = area;
	}

	private Stand() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Stand)) {
			return false;
		}

		Stand stand = (Stand) o;

		if (!description.equals(stand.description)) {
			return false;
		}
		if (!area.equals(stand.area)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = description.hashCode();
		result = 31 * result + area.hashCode();
		return result;
	}
}
