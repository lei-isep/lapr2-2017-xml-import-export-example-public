package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 15/06/16.
 */
@XmlRootElement
public class ApplicationSet {

	@XmlElement(name = "application")
	private List<Application> applications;

	private ApplicationSet() {
	}

	public ApplicationSet(List<Application> applications) {

		this.applications = applications;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ApplicationSet)) {
			return false;
		}

		ApplicationSet that = (ApplicationSet) o;

		return applications.equals(that.applications);

	}

	public void addCandidatura(Application application) {
		if (!applications.contains(application)) {
			applications.add(application);
		}
	}

	@Override
	public int hashCode() {
		return applications.hashCode();
	}
}
