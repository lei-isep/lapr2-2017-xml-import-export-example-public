package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement()
public class User {
	@XmlElement
	private String name;

	@XmlElement
	private String email;

	@XmlElement
	private String username;

	@XmlElement
	private String password;

	public User(String name, String email, String username, String password) {
		this.name = name;
		this.email = email;
		this.username = username;
		this.password = password;
	}

	/**
	 * Hides explicit no-arg default contructor. Needed for jaxb.
	 */
	private User() {

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof User)) {
			return false;
		}

		User that = (User) o;

		if (!name.equals(that.name)) {
			return false;
		}
		if (!email.equals(that.email)) {
			return false;
		}
		if (!username.equals(that.username)) {
			return false;
		}
		return password.equals(that.password);

	}

	@Override
	public int hashCode() {
		int result = name.hashCode();
		result = 31 * result + email.hashCode();
		result = 31 * result + username.hashCode();
		result = 31 * result + password.hashCode();
		return result;
	}
}
