package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Exhibition {
	@XmlElement
	private String title;

	@XmlElement
	private String description;

	@XmlElement
	private Date startDate;

	@XmlElement
	private Date endDate;

	@XmlElement
	private Date submissionStartDate;

	@XmlElement
	private Date submissionEndDate;

	@XmlElement
	private Date conflictManagementEndDate;

	@XmlElement
	private String place;

	@XmlElement
	private OrganiserSet organiserSet;

	@XmlElement
	private StaffSet StaffSet;

	@XmlElement
	private ApplicationSet applicationSet;

	@XmlElement
	private AssignmentSet assignmentSet;


	private Exhibition() {
	}

	public Exhibition(String title, String description, Date startDate, Date endDate, Date submissionStartDate, Date submissionEndDate, Date conflictManagementEndDate, String place, OrganiserSet organiserSet, StaffSet staffSet, ApplicationSet applicationSet, AssignmentSet assignmentSet) {
		this.title = title;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.submissionStartDate = submissionStartDate;
		this.submissionEndDate = submissionEndDate;
		this.conflictManagementEndDate = conflictManagementEndDate;
		this.place = place;
		this.organiserSet = organiserSet;
		this.StaffSet = staffSet;
		this.applicationSet = applicationSet;
		this.assignmentSet = assignmentSet;
	}

	public void addCandidatura(Application application) {
		applicationSet.addCandidatura(application);
		for (Review review : application.getReviews()) {

			assignmentSet.addAtribuicao(review.getAssignment());

		}
	}

	@Override
	public int hashCode() {
		int result = title.hashCode();
		result = 31 * result + description.hashCode();
		result = 31 * result + startDate.hashCode();
		result = 31 * result + endDate.hashCode();
		result = 31 * result + submissionStartDate.hashCode();
		result = 31 * result + submissionEndDate.hashCode();
		result = 31 * result + conflictManagementEndDate.hashCode();
		result = 31 * result + place.hashCode();
		result = 31 * result + organiserSet.hashCode();
		result = 31 * result + StaffSet.hashCode();
		result = 31 * result + applicationSet.hashCode();
		result = 31 * result + assignmentSet.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Exhibition)) {
			return false;
		}

		Exhibition exhibition = (Exhibition) o;

		if (!title.equals(exhibition.title)) {
			return false;
		}
		if (!description.equals(exhibition.description)) {
			return false;
		}
		if (!startDate.equals(exhibition.startDate)) {
			return false;
		}
		if (!endDate.equals(exhibition.endDate)) {
			return false;
		}
		if (!submissionStartDate.equals(exhibition.submissionStartDate)) {
			return false;
		}
		if (!submissionEndDate.equals(exhibition.submissionEndDate)) {
			return false;
		}
		if (!conflictManagementEndDate.equals(exhibition.conflictManagementEndDate)) {
			return false;
		}
		if (!place.equals(exhibition.place)) {
			return false;
		}
		if (!organiserSet.equals(exhibition.organiserSet)) {
			return false;
		}
		if (!StaffSet.equals(exhibition.StaffSet)) {
			return false;
		}
		if (!applicationSet.equals(exhibition.applicationSet)) {
			return false;
		}
		return assignmentSet.equals(exhibition.assignmentSet);

	}
}
