package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement()
public class UserSet {

	@XmlElement(name = "user")
	private List<User> users;

	public UserSet(List<User> users) {
		this.users = users;
	}

	private UserSet() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof UserSet)) {
			return false;
		}

		UserSet that = (UserSet) o;

		return users.equals(that.users);

	}

	@Override
	public int hashCode() {
		return users.hashCode();
	}
}
