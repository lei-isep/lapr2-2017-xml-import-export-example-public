package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class OrganiserSet {
	@XmlElement(name = "organiser")
	private List<Organiser> organisers;

	public OrganiserSet(List<Organiser> organisers) {
		this.organisers = organisers;
	}

	private OrganiserSet() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof OrganiserSet)) {
			return false;
		}

		OrganiserSet that = (OrganiserSet) o;

		return organisers.equals(that.organisers);

	}

	@Override
	public int hashCode() {
		return organisers.hashCode();
	}
}
