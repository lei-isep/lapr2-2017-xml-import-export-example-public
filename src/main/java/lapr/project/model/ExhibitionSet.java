package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class ExhibitionSet {
	@XmlElement(name = "exhibition")
	private List<Exhibition> exhibitions;

	public ExhibitionSet(List<Exhibition> exhibitions) {
		this.exhibitions = exhibitions;
	}

	private ExhibitionSet() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ExhibitionSet)) {
			return false;
		}

		ExhibitionSet that = (ExhibitionSet) o;

		return exhibitions.equals(that.exhibitions);

	}

	@Override
	public int hashCode() {
		return exhibitions.hashCode();
	}
}
