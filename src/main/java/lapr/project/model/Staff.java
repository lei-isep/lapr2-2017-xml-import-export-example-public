package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Staff {

	@XmlElement
	private User user;

	public Staff(User user) {
		this.user = user;
	}

	private Staff() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Staff)) {
			return false;
		}

		Staff staff = (Staff) o;

		return user.equals(staff.user);

	}

	@Override
	public int hashCode() {
		return user.hashCode();
	}

}
