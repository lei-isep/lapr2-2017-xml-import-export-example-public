package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class StaffSet {

	@XmlElement(name = "staff")
	private List<Staff> staffs;

	private StaffSet() {
	}

	public StaffSet(List<Staff> staffs) {
		this.staffs = staffs;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof StaffSet)) {
			return false;
		}

		StaffSet that = (StaffSet) o;

		return staffs.equals(that.staffs);

	}

	@Override
	public int hashCode() {
		return staffs.hashCode();
	}
}
