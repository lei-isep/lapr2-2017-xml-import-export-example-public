package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Application {
	@XmlElement
	private Boolean accepted;

	@XmlElement
	private String description;

	@XmlElement
	private Integer boothArea;

	@XmlElement
	private Integer invitesQuantity;

	@XmlElementWrapper(name = "reviews")
	@XmlElement(name = "review")
	private List<Review> reviews = new ArrayList<>();

	@XmlElementWrapper(name = "topics")
	@XmlElement(name = "topic")
	private List<String> topics = new ArrayList<>();

	private Application() {
	}

	public Application(Boolean accepted, String description, Integer boothArea, Integer invitesQuantity, List<Review> reviews, List<String> topics) {
		this.accepted = accepted;
		this.description = description;
		this.boothArea = boothArea;
		this.invitesQuantity = invitesQuantity;
		this.reviews = reviews;
		this.topics = topics;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Application)) {
			return false;
		}

		Application that = (Application) o;

		if (!accepted.equals(that.accepted)) {
			return false;
		}
		if (!description.equals(that.description)) {
			return false;
		}
		if (!boothArea.equals(that.boothArea)) {
			return false;
		}
		if (!invitesQuantity.equals(that.invitesQuantity)) {
			return false;
		}
		if (!reviews.equals(that.reviews)) {
			return false;
		}
		return topics.equals(that.topics);

	}

	@Override
	public int hashCode() {
		int result = accepted.hashCode();
		result = 31 * result + description.hashCode();
		result = 31 * result + boothArea.hashCode();
		result = 31 * result + invitesQuantity.hashCode();
		result = 31 * result + reviews.hashCode();
		result = 31 * result + topics.hashCode();
		return result;
	}

	public ArrayList<Review> getReviews(){
		return (ArrayList<Review>) reviews;
	}
}
