package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class AssignmentSet {
	@XmlElement(name = "assignment")
	private List<Assignment> assignments;

	public AssignmentSet(List<Assignment> assignments) {
		this.assignments = assignments;
	}

	private AssignmentSet() {
	}

	@Override
	public int hashCode() {
		return assignments.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof AssignmentSet)) {
			return false;
		}

		AssignmentSet that = (AssignmentSet) o;

		return assignments.equals(that.assignments);

	}

	public void addAtribuicao(Assignment assignment) {
		if (!assignments.contains(assignment)) {
			assignments.add(assignment);
		}
	}
}
