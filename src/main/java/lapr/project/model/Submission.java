package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Submission {
	@XmlElement
	private Date submissionStartDate;

	@XmlElement
	private Date submissionEndDate;

	private Submission() {
	}

	public Submission(Date submissionStartDate, Date submissionEndDate) {
		this.submissionStartDate = submissionStartDate;
		this.submissionEndDate = submissionEndDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Submission)) {
			return false;
		}

		Submission submission = (Submission) o;

		if (!submissionStartDate.equals(submission.submissionStartDate)) {
			return false;
		}
		return submissionEndDate.equals(submission.submissionEndDate);

	}

	@Override
	public int hashCode() {
		int result = submissionStartDate.hashCode();
		result = 31 * result + submissionEndDate.hashCode();
		return result;
	}
}
