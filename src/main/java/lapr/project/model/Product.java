package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 16/06/16.
 */
@XmlRootElement
public class Product {

	@XmlElement
	private String name;

	/**
	 * Overide default constructor.
	 */
	private Product() {

	}

	/**
	 * @param productName Product Name
	 */
	public Product(String productName) {
	}

	@Override
	public int hashCode() {
		return name != null ? name.hashCode() : 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Product)) {
			return false;
		}

		Product product = (Product) o;

		return name != null ? name.equals(product.name) : product.name == null;
	}
}
