package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Assignment {
	@XmlElement
	private Staff staff;

	public Assignment(Staff staff/*, Candidatura candidatura*/) {
		this.staff = staff;
	}

	private Assignment() {
	}

	@Override
	public int hashCode() {
		return staff.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Assignment)) {
			return false;
		}

		Assignment that = (Assignment) o;

		return staff.equals(that.staff);

	}

}
