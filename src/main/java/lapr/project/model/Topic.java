package lapr.project.model;

import lapr.project.utils.Exportable;
import lapr.project.utils.Importable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Represents a topic.
 *
 * @author by Nuno Bettencourt [nmb@isep.ipp.pt] on 29/05/16.
 */
public class Topic implements Exportable, Importable<Topic> {

	private static final String ROOT_ELEMENT_NAME = "topic";
	private static final String VALUE_ELEMENT_NAME = "value";

	/**
	 * Topic representation.
	 */
	private String value = "";

	/**
	 * Default empty constructor.
	 */
	public Topic() {

	}

	/**
	 * Constructor for Topic Class.
	 *
	 * @param topic Topic being used.
	 */
	public Topic(String topic) {
		this.value = topic;
	}

	/**
	 * Obtain topic value.
	 *
	 * @return Topic Value
	 */
	private String getValue() {
		return this.value;
	}

	@Override
	public Node exportContentToXMLNode() throws ParserConfigurationException {
		Node node = null;


		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		//Create document builder
		DocumentBuilder builder = factory.newDocumentBuilder();

		//Obtain a new document
		Document document = builder.newDocument();

		//Create root element
		Element elementTopic = document.createElement(ROOT_ELEMENT_NAME);

		//Create a sub-element
		Element elementValue = document.createElement(VALUE_ELEMENT_NAME);

		//Set the sub-element value
		elementValue.setTextContent(getValue());

		//Add sub-element to root element
		elementTopic.appendChild(elementValue);

		//Add root element to document
		document.appendChild(elementTopic);

		node = elementTopic;

		return node;
	}

	@Override
	public Topic importContentFromXMLNode(Node node) throws ParserConfigurationException {

		DocumentBuilderFactory factory =
				DocumentBuilderFactory.newInstance();
		//Create document builder
		DocumentBuilder builder = factory.newDocumentBuilder();

		//Obtain a new document
		Document document = builder.newDocument();

		document.appendChild(document.importNode(node, true));

		NodeList elementsTopic = document.getElementsByTagName(VALUE_ELEMENT_NAME);

		Node elementTopic = elementsTopic.item(0);

		//Get value
		this.value = elementTopic.getFirstChild().getNodeValue();
		return this;
	}

	@Override
	public int hashCode() {
		return getValue().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Topic)) {
			return false;
		}

		Topic that = (Topic) o;

		return getValue().equals(that.getValue());

	}
}
