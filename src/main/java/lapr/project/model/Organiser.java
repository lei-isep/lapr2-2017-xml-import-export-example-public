package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Organiser {
	@XmlElement
	private User user;

	private Organiser() {
	}

	public Organiser(User user) {
		this.user = user;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Organiser)) {
			return false;
		}

		Organiser that = (Organiser) o;

		return user.equals(that.user);

	}

	@Override
	public int hashCode() {
		return user.hashCode();
	}
}
