package lapr.project.model;

import lapr.project.utils.Exportable;
import lapr.project.utils.Importable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Application class.
 *
 * @author by Nuno Bettencourt [nmb@isep.ipp.pt] on 29/05/16.
 */
public class AnotherApplicationExample implements Importable<AnotherApplicationExample>, Exportable {
	private static final String ROOT_ELEMENT_NAME = "application";
	private static final String DESCRIPTION_ELEMENT_NAME = "description";
	private static final String KEYWORDS_ELEMENT_NAME = "topics";
	private final List<Topic> topicList = new ArrayList<>();
	private String description = "";

	/**
	 * Constructor for AnotherApplicationExample
	 *
	 * @param description CandidaturaDescription
	 * @param topicList Topic List
	 */
	public AnotherApplicationExample(String description, List<Topic> topicList) {
		this.description = description;
		this.topicList.addAll(topicList);
	}

	/**
	 * Default public constructor.
	 */
	public AnotherApplicationExample() {

	}

	/**
	 * Obtain Application's description.
	 *
	 * @return Application description
	 */
	private String getDescription() {
		return description;
	}

	/**
	 * Add a topic to Application.
	 *
	 * @param topic Topic to be added.
	 */
	public void addTopic(Topic topic) {
		getTopicList().add(topic);
	}

	/**
	 * Obtain the list of existing topics.
	 *
	 * @return A list of existing topics.
	 */
	public List<Topic> getTopicList() {
		return topicList;

	}

	@Override
	public Node exportContentToXMLNode() throws ParserConfigurationException {
		Node rootNode = null;

		DocumentBuilderFactory factory =
				DocumentBuilderFactory.newInstance();
		//Create document builder
		DocumentBuilder builder = factory.newDocumentBuilder();

		//Obtain a new document
		Document document = builder.newDocument();

		//Create root element
		Element elementCandidatura = document.createElement(ROOT_ELEMENT_NAME);

		//Create a sub-element
		Element elementDescription = document.createElement(DESCRIPTION_ELEMENT_NAME);

		//Set the sub-element value
		elementDescription.setTextContent(getDescription());

		//Add sub-element to root element
		elementCandidatura.appendChild(elementDescription);

		//Create a sub-element
		Element elementTopics = document.createElement(KEYWORDS_ELEMENT_NAME);
		elementCandidatura.appendChild(elementTopics);

		//iterate over topics
		for (Topic topic : getTopicList()
				) {
			Node topicNode = topic.exportContentToXMLNode();
			elementTopics.appendChild(document.importNode(topicNode, true));
		}

		//Add root element to document
		document.appendChild(elementCandidatura);

		//It exports only the element representation to XMÇ, ommiting the XML header
		rootNode = elementCandidatura;

		return rootNode;
	}

	@Override
	public AnotherApplicationExample importContentFromXMLNode(Node node) throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		//Create document builder
		DocumentBuilder builder = factory.newDocumentBuilder();

		//Obtain a new document
		Document document = builder.newDocument();

		document.appendChild(document.importNode(node, true));

		NodeList elementsCandidatura = document.getElementsByTagName(ROOT_ELEMENT_NAME);

		Node elementCandidatura = elementsCandidatura.item(0);

		//Get description
		this.description = elementCandidatura.getFirstChild().getFirstChild().getNodeValue();

		NodeList elementsTopics = document.getElementsByTagName(KEYWORDS_ELEMENT_NAME);

		NodeList topics = elementsTopics.item(0).getChildNodes();
		for (int position = 0; position < topics.getLength(); position++) {
			Node nodeTopic = topics.item(position);
			Topic newTopic = new Topic();

			newTopic = newTopic.importContentFromXMLNode(nodeTopic);
			addTopic(newTopic);
		}

		return this;
	}

	@Override
	public int hashCode() {
		int result = getDescription().hashCode();
		result = 31 * result + getTopicList().hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof AnotherApplicationExample)) {
			return false;
		}

		AnotherApplicationExample that = (AnotherApplicationExample) o;

		if (!getDescription().equals(that.getDescription())) {
			return false;
		}
		return getTopicList().equals(that.getTopicList());

	}
}
