package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Stand {

	@XmlElement
	private String description;

	public Stand(String description) {
		this.description = description;
	}

	private Stand() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Stand)) {
			return false;
		}

		Stand stand = (Stand) o;

		return description.equals(stand.description);

	}

	@Override
	public int hashCode() {
		return description.hashCode();
	}
}
